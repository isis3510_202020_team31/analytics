var express = require('express');
var router = express.Router();
require('dotenv').config();
var admin = require("firebase-admin");

var serviceAccount = require(process.env.FILE_PATH);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.URL
});
const db = admin.firestore();
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/reservation', function(req , res , next){
  let data =[];
  db.collection('reservations').get().then(snapshot => {
    snapshot.forEach(doc => {
      data.push(doc.data());      
    });
  })
  .catch(err => {
    console.log('Error getting documents', err);
  }).finally(()=>{
    res.send(data);
  });  
});
module.exports = router;
