import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GraphincTelemetryComponent} from './telemetry/graphinc-telemetry/graphinc-telemetry.component';
import {AppComponent} from './app.component';
import {DataGraphicsComponent} from './data-questions/data-graphics/data-graphics.component';
import {HomeViewComponent} from './home/home-view/home-view.component';
import {FeaturesGraphicsComponent} from './new-deprecated-features/features-graphics/features-graphics.component';
import { UserExperienceGraphicsComponent} from './user-experience/user-experience-graphics/user-experience-graphics.component';
import {MixedGraphicsComponent} from './mixed-questions/mixed-graphics/mixed-graphics.component';

const routes: Routes = [

  {
    path: 'home',
    component: HomeViewComponent
  },
  {
    path: 'telemetry',
    component: GraphincTelemetryComponent
  },
  {
    path: 'dataQuestions',
    component: DataGraphicsComponent
  },
  {
    path: 'newDeprecatedFeatures',
    component: FeaturesGraphicsComponent
  },
  {
    path: 'userExperience',
    component: UserExperienceGraphicsComponent
  },
  {
    path: 'mixedQuestions',
    component: MixedGraphicsComponent
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
