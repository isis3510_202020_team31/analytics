import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TelemetryModule } from './telemetry/telemetry.module';
import { DataQuestionsModule} from './data-questions/data-questions.module';
import { HomeModule} from './home/home.module';
import { MixedQuestionsModule} from './mixed-questions/mixed-questions.module';
import { UserExperienceModule} from './user-experience/user-experience.module';
import { NewDeprecatedFeaturesModule} from './new-deprecated-features/new-deprecated-features.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TelemetryModule,
    DataQuestionsModule,
    AppRoutingModule,
    HomeModule,
    MixedQuestionsModule,
    UserExperienceModule,
    NewDeprecatedFeaturesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
