import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataGraphicsComponent } from './data-graphics/data-graphics.component';



@NgModule({
  declarations: [DataGraphicsComponent],
  imports: [
    CommonModule
  ]
})
export class DataQuestionsModule { }
