import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MixedGraphicsComponent } from './mixed-graphics.component';

describe('MixedGraphicsComponent', () => {
  let component: MixedGraphicsComponent;
  let fixture: ComponentFixture<MixedGraphicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MixedGraphicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MixedGraphicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
