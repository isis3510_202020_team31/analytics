import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MixedGraphicsComponent } from './mixed-graphics/mixed-graphics.component';



@NgModule({
  declarations: [MixedGraphicsComponent],
  imports: [
    CommonModule
  ]
})
export class MixedQuestionsModule { }
