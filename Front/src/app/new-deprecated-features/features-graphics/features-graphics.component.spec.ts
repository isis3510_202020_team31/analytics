import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturesGraphicsComponent } from './features-graphics.component';

describe('FeaturesGraphicsComponent', () => {
  let component: FeaturesGraphicsComponent;
  let fixture: ComponentFixture<FeaturesGraphicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeaturesGraphicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturesGraphicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
