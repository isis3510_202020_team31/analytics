import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturesGraphicsComponent } from './features-graphics/features-graphics.component';



@NgModule({
  declarations: [FeaturesGraphicsComponent],
  imports: [
    CommonModule
  ]
})
export class NewDeprecatedFeaturesModule { }
