import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphincTelemetryComponent } from './graphinc-telemetry.component';

describe('GraphincTelemetryComponent', () => {
  let component: GraphincTelemetryComponent;
  let fixture: ComponentFixture<GraphincTelemetryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraphincTelemetryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphincTelemetryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
