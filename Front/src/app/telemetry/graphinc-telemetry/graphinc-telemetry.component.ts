import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';


@Component({
  selector: 'app-graphinc-telemetry',
  styleUrls: ['./graphinc-telemetry.component.css'],
  templateUrl: './graphinc-telemetry.component.html'
})
export class GraphincTelemetryComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }
}


