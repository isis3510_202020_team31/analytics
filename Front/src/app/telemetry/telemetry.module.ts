import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphincTelemetryComponent } from './graphinc-telemetry/graphinc-telemetry.component';
import {ChartsModule} from 'ng2-charts';
import { TableauModule } from 'ngx-tableau';



@NgModule({
  declarations: [GraphincTelemetryComponent],
  imports: [
    CommonModule,
    ChartsModule,
    TableauModule
  ]
})
export class TelemetryModule { }
