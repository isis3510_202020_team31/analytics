import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserExperienceGraphicsComponent } from './user-experience-graphics.component';

describe('UserExperienceGraphicsComponent', () => {
  let component: UserExperienceGraphicsComponent;
  let fixture: ComponentFixture<UserExperienceGraphicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserExperienceGraphicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserExperienceGraphicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
