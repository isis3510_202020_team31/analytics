import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserExperienceGraphicsComponent } from './user-experience-graphics/user-experience-graphics.component';



@NgModule({
  declarations: [UserExperienceGraphicsComponent],
  imports: [
    CommonModule
  ]
})
export class UserExperienceModule { }
